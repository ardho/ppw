from django.urls import re_path
from . import views

app_name = 's5'

urlpatterns = [
    re_path(r'^$', views.mySchedule),
    re_path(r'^addSchedule', views.addSchedule, name='addSchedule'),
    re_path(r'^wipeSchedule', views.deleteAll),
]