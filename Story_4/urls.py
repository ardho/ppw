from django.urls import re_path
from .views import index, register, schedule

urlpatterns = [
    re_path(r'^$', index),
    re_path(r'^register.html$', register),
    re_path(r'^schedule.html$', schedule),
]
